﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public static class MemberInfoExtensions
    {

        #region Get Custom Attributes

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="element"></param>
        /// <param name="inherit"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetCustomAttributes<T>(this MemberInfo self, bool inherit) where T : Attribute
        {
            return (IEnumerable<T>)CustomAttributeExtensions.GetCustomAttributes(self, typeof(T), inherit);
        }

        #endregion

    }

}
