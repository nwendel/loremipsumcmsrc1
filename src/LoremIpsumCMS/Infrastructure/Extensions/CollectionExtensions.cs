﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public static class CollectionExtensions
    {

        #region Add Range

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="items"></param>
        public static void AddRange<T>(this ICollection<T> self, IEnumerable<T> items)
        {
            foreach(var item in items)
            {
                self.Add(item);
            }
        }

        #endregion

        #region Remove Range

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="items"></param>
        public static void RemoveRange<T>(this ICollection<T> self, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                self.Remove(item);
            }
        }

        #endregion

        #region Remove Where

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="predicate"></param>
        public static void RemoveWhere<T>(this ICollection<T> self, Func<T, bool> predicate)
        {
            var remove = self.Where(predicate).ToList();
            self.RemoveRange(remove);
        }

        #endregion

    }

}
