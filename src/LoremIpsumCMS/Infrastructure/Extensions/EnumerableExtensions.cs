﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public static class EnumerableExtensions
    {
        #region None

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool None<T>(this IEnumerable<T> self)
        {
            return !self.Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="self"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool None<TSource>(this IEnumerable<TSource> self, Func<TSource, bool> predicate)
        {
            return !self.Any(predicate);
        }

        #endregion

    }

}
