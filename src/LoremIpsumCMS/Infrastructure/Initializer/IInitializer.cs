﻿namespace LoremIpsumCMS.Infrastructure.Initializer
{

    /// <summary>
    /// 
    /// </summary>
    public interface IInitializer
    {

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        void Initialize();

        #endregion

    }

}
