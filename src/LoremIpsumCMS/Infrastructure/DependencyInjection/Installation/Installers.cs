﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Installation
{

    /// <summary>
    /// 
    /// </summary>
    public class Installers
    {

        #region From Assembly

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static IInstaller[] FromAssembly(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException(nameof(assembly));
            }

            var allTypes = assembly.GetTypes();
            var installerTypes = allTypes
                .Where(x => typeof(IInstaller).IsAssignableFrom(x))
                .Select(x => Activator.CreateInstance(x))
                .Cast<IInstaller>()
                .ToArray();
            return installerTypes;
        }

        #endregion

        #region From This Assembly

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static IInstaller[] FromThisAssembly()
        {
            return FromAssembly(Assembly.GetCallingAssembly());
        }

        #endregion


    }

}
