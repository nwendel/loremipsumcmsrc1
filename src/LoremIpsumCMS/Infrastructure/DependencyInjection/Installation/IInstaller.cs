﻿using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Installation
{

    /// <summary>
    /// 
    /// </summary>
    public interface IInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        void Install(IServiceCollection serviceCollection);

        #endregion

    }

}
