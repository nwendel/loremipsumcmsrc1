﻿using System;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using LoremIpsumCMS.Infrastructure.Extensions;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class RegistrationFromDescriptor : IRegistration
    {

        #region Fields

        private Predicate<Type> _filter;
        private ICollection<RegistrationBasedOnDescriptor> _criterias;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        protected RegistrationFromDescriptor(Predicate<Type> filter)
        {
            _filter = filter;

            _criterias = new Collection<RegistrationBasedOnDescriptor>();
        }

        #endregion

        #region Selected Types

        /// <summary>
        /// 
        /// </summary>
        protected abstract IEnumerable<Type> SelectedTypes { get; }

        #endregion

        #region Based On

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public RegistrationBasedOnDescriptor BasedOn<T>()
        {
            return BasedOn(typeof(T));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basedOn"></param>
        /// <returns></returns>
        public RegistrationBasedOnDescriptor BasedOn(Type basedOn)
        {
            return BasedOn((IEnumerable<Type>)new[] { basedOn });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basedOn"></param>
        /// <returns></returns>
        public RegistrationBasedOnDescriptor BasedOn(params Type[] basedOn)
        {
            return BasedOn((IEnumerable<Type>)basedOn);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basedOn"></param>
        /// <returns></returns>
        public RegistrationBasedOnDescriptor BasedOn(IEnumerable<Type> basedOn)
        {
            var descriptor = new RegistrationBasedOnDescriptor(basedOn, this, _filter);
            _criterias.Add(descriptor);
            return descriptor;
        }
        #endregion

        #region Where

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accepted"></param>
        /// <returns></returns>
        public RegistrationBasedOnDescriptor Where(Predicate<Type> accepted)
        {
            var descriptor = new RegistrationBasedOnDescriptor(new[] { typeof(object) }, this, _filter).If(accepted);
            _criterias.Add(descriptor);
            return descriptor;
        }

        #endregion

        #region Register

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        void IRegistration.Register(IServiceCollection serviceCollection)
        {
            if (_criterias.None())
            {
                return;
            }

            foreach (var type in SelectedTypes)
            {
                foreach (var criteria in _criterias)
                {
                    if (criteria.TryRegister(type, serviceCollection))
                    {
                        break;
                    }
                }
            }
        }

        #endregion

    }

}
