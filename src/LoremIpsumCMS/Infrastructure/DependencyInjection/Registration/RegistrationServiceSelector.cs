﻿using System;
using System.Collections.Generic;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public delegate IEnumerable<Type> RegistrationServiceSelector(Type type);

}
