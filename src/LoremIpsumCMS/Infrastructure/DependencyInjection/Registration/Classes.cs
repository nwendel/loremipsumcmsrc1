﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public static class Classes
    {

        #region From Assembly

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static RegistrationFromAssemblyDescriptor FromAssembly(Assembly assembly)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException(nameof(assembly));
            }
            return new RegistrationFromAssemblyDescriptor(assembly, Filter);
        }

        #endregion

        #region From This Assembly

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static RegistrationFromAssemblyDescriptor FromThisAssembly()
        {
            return FromAssembly(Assembly.GetCallingAssembly());
        }

        #endregion

        #region Filter

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        internal static bool Filter(Type type)
        {
            return
                type.IsClass &&
                !type.IsAbstract;
            //&& !type.IsNestedPrivate;
        }
        #endregion

    }

}
