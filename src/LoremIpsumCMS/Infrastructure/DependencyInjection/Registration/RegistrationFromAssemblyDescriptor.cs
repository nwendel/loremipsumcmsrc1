﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public class RegistrationFromAssemblyDescriptor : RegistrationFromDescriptor
    {

        #region Fields

        private IEnumerable<Assembly> _assemblies;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="filter"></param>
        protected internal RegistrationFromAssemblyDescriptor(Assembly assembly, Predicate<Type> filter) : base(filter)
        {
            _assemblies = new[] { assembly };
        }

        #endregion

        #region Selected Types

        /// <summary>
        /// 
        /// </summary>
        /// <param name="kernel"></param>
        /// <returns></returns>
        protected override IEnumerable<Type> SelectedTypes
        {
            get
            {
                return _assemblies.SelectMany(a => a.GetTypes());
            }
        }

        #endregion

    }

}
