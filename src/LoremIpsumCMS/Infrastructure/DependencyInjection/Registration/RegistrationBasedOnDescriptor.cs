﻿using LoremIpsumCMS.Infrastructure.Extensions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public class RegistrationBasedOnDescriptor : IRegistration
    {

        #region Fields

        private Predicate<Type> _ifFilter;
        private IList<Type> _basedOn;
        private RegistrationFromDescriptor _fromDescriptor;
        private RegistrationServiceDescriptor _serviceDescriptor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basedOn"></param>
        /// <param name="fromDescriptor"></param>
        /// <param name="additionalFilters"></param>
        internal RegistrationBasedOnDescriptor(IEnumerable<Type> basedOn, RegistrationFromDescriptor fromDescriptor, Predicate<Type> additionalFilters)
        {
            _basedOn = basedOn.ToList();
            _fromDescriptor = fromDescriptor;
            If(additionalFilters);

            _serviceDescriptor = new RegistrationServiceDescriptor(this);
        }

        #endregion

        #region With Service

        /// <summary>
        /// 
        /// </summary>
        public RegistrationServiceDescriptor WithService => _serviceDescriptor;

        #endregion

        #region If

        /// <summary>
        ///   Assigns a conditional predication which must be satisfied.
        /// </summary>
        /// <param name = "ifFilter"> The predicate to satisfy. </param>
        /// <returns> </returns>
        public RegistrationBasedOnDescriptor If(Predicate<Type> ifFilter)
        {
            _ifFilter += ifFilter;
            return this;
        }

        #endregion

        #region Register

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        void IRegistration.Register(IServiceCollection serviceCollection)
        {
            var registration = (IRegistration)_fromDescriptor;
            registration.Register(serviceCollection);
        }

        #endregion

        #region Try Register

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="serviceCollection"></param>
        /// <returns></returns>
        internal bool TryRegister(Type type, IServiceCollection serviceCollection)
        {
            Type[] types;
            if(!Accepts(type, out types))
            {
                return false;
            }

            var serviceTypes = _serviceDescriptor.GetServices(type, types);

            // TODO: This is not present in the code I have copied...
            if(serviceTypes.None())
            {
                return false;
            }

            var registration = Component.For(serviceTypes.ToArray());
            registration.ImplementedBy(type);

            serviceCollection.Register(registration);

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        protected virtual bool Accepts(Type type, out Type[] types)
        {
            return 
                IsBasedOn(type, out types) &&
                ExecuteIfCondition(type);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected bool ExecuteIfCondition(Type type)
        {
            if (_ifFilter == null)
            {
                return true;
            }

            foreach(var filter in _ifFilter.GetInvocationList().Cast<Predicate<Type>>())
            {
                if(!filter(type))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="baseTypes"></param>
        /// <returns></returns>
        protected bool IsBasedOn(Type type, out Type[] baseTypes)
        {
            var actuallyBasedOn = new List<Type>();
            foreach (var potentialBase in _basedOn)
            {
                if (potentialBase.IsAssignableFrom(type))
                {
                    actuallyBasedOn.Add(potentialBase);
                }
                else if (potentialBase.IsGenericTypeDefinition)
                {
                    if (potentialBase.IsInterface)
                    {
                        if (IsBasedOnGenericInterface(type, potentialBase, out baseTypes))
                        {
                            actuallyBasedOn.AddRange(baseTypes);
                        }
                    }

                    if (IsBasedOnGenericClass(type, potentialBase, out baseTypes))
                    {
                        actuallyBasedOn.AddRange(baseTypes);
                    }
                }
            }
            baseTypes = actuallyBasedOn.Distinct().ToArray();
            return baseTypes.Length > 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="basedOn"></param>
        /// <param name="baseTypes"></param>
        /// <returns></returns>
        private static bool IsBasedOnGenericClass(Type type, Type basedOn, out Type[] baseTypes)
        {
            while (type != null)
            {
                if (type.IsGenericType && type.GetGenericTypeDefinition() == basedOn)
                {
                    baseTypes = new[] { type };
                    return true;
                }

                type = type.BaseType;
            }
            baseTypes = null;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="basedOn"></param>
        /// <param name="baseTypes"></param>
        /// <returns></returns>
        private static bool IsBasedOnGenericInterface(Type type, Type basedOn, out Type[] baseTypes)
        {
            var types = new List<Type>(4);
            foreach (var @interface in type.GetInterfaces())
            {
                if (@interface.IsGenericType &&
                    @interface.GetGenericTypeDefinition() == basedOn)
                {
                    if (@interface.ReflectedType == null &&
                        @interface.ContainsGenericParameters)
                    {
                        types.Add(@interface.GetGenericTypeDefinition());
                    }
                    else
                    {
                        types.Add(@interface);
                    }
                }
            }
            baseTypes = types.ToArray();
            return baseTypes.Length > 0;
        }

        #endregion

    }

}
