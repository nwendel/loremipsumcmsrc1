﻿using LoremIpsumCMS.Infrastructure.Extensions;
using System;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public static class Component
    {

        #region For

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public static ComponentRegistration For(Type serviceType)
        {
            if (serviceType == null)
            {
                throw new ArgumentNullException(nameof(serviceType), "The argument was null. Check that the assembly is referenced and the type available to your application.");
            }

            return new ComponentRegistration(serviceType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceTypes"></param>
        /// <returns></returns>
        public static ComponentRegistration For(params Type[] serviceTypes)
        {
            if (serviceTypes.None())
            {
                throw new ArgumentException("At least one service type must be supplied");
            }

            return new ComponentRegistration(serviceTypes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ComponentRegistration<T> For<T>()
        {
            return new ComponentRegistration<T>();
        }

        #endregion

        #region For All Interfaces

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ComponentRegistration ForAllInterfaces()
        {
            return new ComponentRegistration();
        }

        #endregion

        #region Is In Namespace

        /// <summary>
        /// 
        /// </summary>
        /// <param name="namespace"></param>
        /// <returns></returns>
        public static Predicate<Type> IsInNamespace(string @namespace)
        {
            return type => type.Namespace == @namespace;
        }

        #endregion

        #region Is In Same Namespace As

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Predicate<Type> IsInSameNamespaceAs(Type type)
        {
            return IsInNamespace(type.Namespace);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Predicate<Type> IsInSameNamespaceAs<T>()
        {
            return IsInSameNamespaceAs(typeof(T));
        }

        #endregion

    }

}
