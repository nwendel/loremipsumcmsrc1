﻿using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public interface IRegistration
    {

        #region Register

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        void Register(IServiceCollection serviceCollection);

        #endregion

    }

}
