﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public static class Extensions
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static Type[] GetAllInterfaces(this Type self)
        {
            var types = GetAllInterfaces(new[] { self });
            return types;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        private static Type[] GetAllInterfaces(params Type[] types)
        {
            if (types == null)
            {
                return Type.EmptyTypes;
            }

            var hashSet = new HashSet<Type>();

            for (int index = 0; index < types.Length; ++index)
            {
                var type1 = types[index];
                if (type1 != null && (!type1.IsInterface || hashSet.Add(type1)))
                {
                    foreach (Type type2 in type1.GetInterfaces())
                    {
                        hashSet.Add(type2);
                    }
                }
            }
            return Sort(hashSet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="types"></param>
        /// <returns></returns>
        private static Type[] Sort(ICollection<Type> types)
        {
            var array = new Type[types.Count];
            types.CopyTo(array, 0);
            Array.Sort(array, ((l, r) => string.Compare(l.AssemblyQualifiedName, r.AssemblyQualifiedName, StringComparison.OrdinalIgnoreCase)));
            return array;
        }

    }

}
