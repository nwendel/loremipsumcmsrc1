﻿using System;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public class ComponentRegistration<TService> : IRegistration
    {

        #region Fields

        private Type[] _services;
        private RegistrationServiceSelector _serviceSelector;
        private Type _implementedBy;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public ComponentRegistration() : this(typeof(TService))
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public ComponentRegistration(params Type[] services)
        {
            _services = services;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceSelector"></param>
        public ComponentRegistration(RegistrationServiceSelector serviceSelector)
        {
            _serviceSelector = serviceSelector;
        }

        #endregion

        #region Implemented By

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ComponentRegistration<TService> ImplementedBy<TImplementation>()
            where TImplementation : TService
        {
            return ImplementedBy(typeof(TImplementation));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ComponentRegistration<TService> ImplementedBy(Type type)
        {
            _implementedBy = type;
            return this;
        }

        #endregion

        #region Register

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        void IRegistration.Register(IServiceCollection serviceCollection)
        {
            // TODO: Not sure about this either, can it cause problems?
            if(serviceCollection.Any(x => x.ImplementationType == _implementedBy))
            {
                // This class already has implementation...
                return;
            }

            var services = _services ?? _serviceSelector(_implementedBy);

            var service = services.First();
            var otherServices = services.Skip(1);

            serviceCollection.AddSingleton(service, _implementedBy);
            foreach(var otherService in otherServices)
            {
                serviceCollection.AddSingleton(otherService, x => x.GetService(service));
            }
        }

        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    public class ComponentRegistration : ComponentRegistration<object>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public ComponentRegistration() : base(x => x.GetAllInterfaces())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceTypes"></param>
        public ComponentRegistration(params Type[] serviceTypes) : base(serviceTypes)
        {
        }

        #endregion

    }

}
