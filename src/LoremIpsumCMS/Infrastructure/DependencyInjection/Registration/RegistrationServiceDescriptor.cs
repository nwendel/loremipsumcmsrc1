﻿using LoremIpsumCMS.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public class RegistrationServiceDescriptor
    {

        #region Fields

        private RegistrationBasedOnDescriptor _basedOnDescriptor;
        private RegistrationServiceSelector _serviceSelector;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basedOnDescriptor"></param>
        internal RegistrationServiceDescriptor(RegistrationBasedOnDescriptor basedOnDescriptor)
        {
            _basedOnDescriptor = basedOnDescriptor;
        }

        #endregion

        #region All Interfaces

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public RegistrationBasedOnDescriptor AllInterfaces()
        {
            return Select(x => x.GetAllInterfaces());
        }

        #endregion

        #region Select

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceSelector"></param>
        /// <returns></returns>
        public RegistrationBasedOnDescriptor Select(RegistrationServiceSelector serviceSelector)
        {
            _serviceSelector += serviceSelector;
            return _basedOnDescriptor;
        }

        #endregion

        #region Get Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="baseTypes"></param>
        /// <returns></returns>
        internal ICollection<Type> GetServices(Type type, Type[] baseTypes)
        {
            var services = new HashSet<Type>();
            if (_serviceSelector != null)
            {
                var selected = _serviceSelector(type);
                if(selected != null)
                {
                    services.AddRange(selected.Select(WorkaroundCLRBug));
                }
            }
            return services;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        private static Type WorkaroundCLRBug(Type serviceType)
        {
            if (!serviceType.IsInterface)
            {
                return serviceType;
            }
            // This is a workaround for a CLR bug in
            // which GetInterfaces() returns interfaces
            // with no implementations.
            if (serviceType.IsGenericType && serviceType.ReflectedType == null)
            {
                var shouldUseGenericTypeDefinition = false;
                foreach (var argument in serviceType.GetGenericArguments())
                {
                    shouldUseGenericTypeDefinition |= argument.IsGenericParameter;
                }
                if (shouldUseGenericTypeDefinition)
                {
                    return serviceType.GetGenericTypeDefinition();
                }
            }
            return serviceType;
        }

        #endregion

    }

}
