﻿using LoremIpsumCMS.Infrastructure.DependencyInjection.Installation;
using LoremIpsumCMS.Infrastructure.DependencyInjection.Registration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LoremIpsumCMS.Infrastructure.DependencyInjection
{

    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        #region Register

        /// <summary>
        /// 
        /// </summary>
        /// <param name="registrations"></param>
        public static void Register(this IServiceCollection self, params IRegistration[] registrations)
        {
            if (self == null)
            {
                throw new ArgumentNullException(nameof(self));
            }
            if (registrations == null)
            {
                throw new ArgumentNullException(nameof(registrations));
            }

            foreach (var registration in registrations)
            {
                registration.Register(self);
            }
        }

        #endregion

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="installers"></param>
        public static void Install(this IServiceCollection self, params IInstaller[] installers)
        {
            if (self == null)
            {
                throw new ArgumentNullException(nameof(self));
            }
            if (installers == null)
            {
                throw new ArgumentNullException(nameof(installers));
            }

            foreach (var installer in installers)
            {
                installer.Install(self);
            }
        }

        #endregion

    }

}
