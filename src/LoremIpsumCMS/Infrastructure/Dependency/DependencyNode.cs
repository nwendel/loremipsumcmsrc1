﻿using System;
using System.Collections.Generic;

namespace LoremIpsumCMS.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    public class DependencyNode<T>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public T Instance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IList<DependsOnNode> DependsOn { get; set; }

        #endregion

    }

}
