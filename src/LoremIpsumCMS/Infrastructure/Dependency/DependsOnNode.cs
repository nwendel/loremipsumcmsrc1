﻿using System;

namespace LoremIpsumCMS.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    public class DependsOnNode
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsOptional { get; set; }

        #endregion

    }

}

