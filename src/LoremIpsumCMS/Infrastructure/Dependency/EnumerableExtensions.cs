﻿using LoremIpsumCMS.Infrastructure.Extensions;
using Raven.Client.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    public static class EnumerableExtensions
    {

        #region Order By Dependencies

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IEnumerable<T> OrderByDependencies<T>(this IEnumerable<T> self)
        {
            var nodes = CreateDependencyNodes(self);

            var result = new Collection<T>();
            while (nodes.Any())
            {
                var nextNodes = nodes
                    .Where(x => x.DependsOn.None())
                    .ToList();

                if (nextNodes.None())
                {
                    var circle = FindCircle(nodes);
                    var circleText = string.Join(" -> ", circle.Select(x => x.FullName));
                    throw new DependencyException(string.Format("Found dependency circle: {0}", circleText));
                }

                nodes = nodes
                    .Where(x => x.DependsOn.Any())
                    .ToList();

                foreach (var nextItem in nextNodes)
                {
                    result.Add(nextItem.Instance);
                    foreach (var node in nodes)
                    {
                        var safeNextItem = nextItem;
                        node.DependsOn.RemoveWhere(x => x.Type == safeNextItem.Type);
                    }
                }
            }

            return result;
        }

        #endregion

        #region Create Dependency Nodes

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        private static IList<DependencyNode<T>> CreateDependencyNodes<T>(IEnumerable<T> items)
        {
            var nodes = items
                .Select(item => new DependencyNode<T>
                {
                    Type = item.GetType(),
                    Instance = item,
                    DependsOn = item.GetType()
                        .GetCustomAttributes<DependsOnAttribute>(true)
                        .Select(t => new DependsOnNode { Type = t.Type, IsOptional = t.IsOptional })
                        .ToList()
                })
                .ToList();

            CheckForInvalidDependency(nodes);

            return nodes;
        }

        #endregion

        #region Check For Invalid Dependency

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodes"></param>
        private static void CheckForInvalidDependency<T>(IList<DependencyNode<T>> nodes)
        {
            var itemTypes = nodes
                .Select(x => x.Type)
                .ToList();

            foreach (var node in nodes)
            {
                node.DependsOn.RemoveWhere(x => x.IsOptional && !x.Type.In(itemTypes));

                foreach (var dependsOn in node.DependsOn)
                {
                    var dependsOnType = dependsOn.Type;
                    if (!dependsOnType.In(itemTypes))
                    {
                        throw new DependencyException(
                            string.Format("{0} depends on {1} which cannot be found",
                                node.Type.Name,
                                dependsOn.Type.Name));
                    }
                }
            }
        }

        #endregion

        #region Find Circle

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodes"></param>
        /// <returns></returns>
        private static IEnumerable<Type> FindCircle<T>(IList<DependencyNode<T>> nodes)
        {
            var visited = Visit(nodes, new[] { nodes.First().Type });

            while(visited.First() != visited.Last())
            {
                visited.RemoveAt(0);
            }

            return visited;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="nodes"></param>
        /// <param name="visited"></param>
        /// <returns></returns>
        private static IList<Type> Visit<T>(IList<DependencyNode<T>> nodes, IList<Type> visited)
        {
            var lastNode = visited.Last();
            foreach (var next in nodes.First(x => x.Type == lastNode).DependsOn)
            {
                if(visited.Contains(next.Type))
                {
                    return visited.Concat(new[] { next.Type }).ToList();
                }
                
                var result = Visit(nodes, visited.Concat(new[] { next.Type }).ToList());
                if(result != null)
                {
                    return result;
                }
            }

            return null;
        }

        #endregion

    }

}
