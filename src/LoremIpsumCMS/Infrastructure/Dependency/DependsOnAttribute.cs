﻿using System;

namespace LoremIpsumCMS.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class DependsOnAttribute : Attribute
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public DependsOnAttribute(Type type)
        {
            Type = type;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Type Type { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsOptional { get; set; }

        #endregion

    }

}
