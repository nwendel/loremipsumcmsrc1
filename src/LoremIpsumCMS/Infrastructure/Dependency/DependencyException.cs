﻿using System;

namespace LoremIpsumCMS.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class DependencyException : Exception
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public DependencyException(string message)
            : base(message)
        {
        }

        #endregion

    }

}
