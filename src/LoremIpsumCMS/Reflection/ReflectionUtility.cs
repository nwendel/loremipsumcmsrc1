﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Reflection
{

    /// <summary>
    /// 
    /// </summary>
    public static class ReflectionUtility
    {

        #region Fields

        //private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Get Application And Framework Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> GetApplicationAndFrameworkTypes()
        {
            return GetApplicationTypes().Concat(GetFrameworkTypes()).ToList();
        }

        #endregion

        #region Get Framework Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Assembly> GetFrameworkAssemblies()
        {
            var rootAssembly = typeof(ReflectionUtility).Assembly;
            var index = rootAssembly.FullName.IndexOfAny(new[] { '.', ',' });

            var frameworkName = rootAssembly.FullName.Substring(0, index);

            var assemblies = GetLoadedAssemblies();

            var frameworkAssemblies = assemblies
                .Where(assembly => IsFrameworkAssembly(frameworkName, assembly.FullName))
                .ToList();
            //_logger.Debug("Found {0} framework assemblies", frameworkAssemblies.Count);
            foreach (var assembly in frameworkAssemblies)
            {
                //_logger.Debug("{0}", assembly.FullName);
            }

            return frameworkAssemblies;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Assembly> GetLoadedAssemblies()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();

            //_logger.Debug("Found {0} loaded assemblies", assemblies.Length);
            foreach (var assembly in assemblies)
            {
                //_logger.Debug("{0}", assembly.FullName);
            }

            return assemblies;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frameworkName"></param>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        private static bool IsFrameworkAssembly(string frameworkName, string assemblyName)
        {
            return assemblyName.StartsWith(frameworkName);
        }

        #endregion

        #region Get Framework Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> GetFrameworkTypes()
        {
            var frameworkAssemblies = GetFrameworkAssemblies();
            var frameworkTypes = frameworkAssemblies
                .SelectMany(x => x.GetTypes())
                .ToList();

            //_logger.Debug("Found {0} framework types", frameworkTypes.Count);

            return frameworkTypes;
        }

        #endregion

        #region Get Application Assemblies

        private static Assembly _applicationRootAssembly;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Assembly> GetApplicationAssemblies()
        {
            var stackFrames = new StackTrace().GetFrames();
            var methods = stackFrames.Select(x => x.GetMethod()).ToList();
            var applicationStartMethod = methods.SingleOrDefault(x => x.Name == "Application_Start");

            if (applicationStartMethod == null && _applicationRootAssembly == null)
            {
                return new Assembly[0];
            }

            var rootAssembly = applicationStartMethod != null
                ? applicationStartMethod.DeclaringType.Assembly
                : _applicationRootAssembly;

            if (_applicationRootAssembly == null)
            {
                _applicationRootAssembly = rootAssembly;
            }

            var index = rootAssembly.FullName.IndexOfAny(new[] { '.', ',' });

            var applicationName = rootAssembly.FullName.Substring(0, index);

            var assemblies = GetLoadedAssemblies();

            var applicationAssemblies = assemblies
                .Where(assembly => IsFrameworkAssembly(applicationName, assembly.FullName))
                .ToList();
            //_logger.Debug("Found {0} application assemblies", applicationAssemblies.Count);
            foreach (var assembly in applicationAssemblies)
            {
                //_logger.Debug("{0}", assembly.FullName);
            }

            return applicationAssemblies;
        }

        #endregion

        #region Get Application Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> GetApplicationTypes()
        {
            var applicationAssemblies = GetApplicationAssemblies();
            var applicationTypes = applicationAssemblies
                .SelectMany(x => x.GetTypes())
                .ToList();

            //_logger.Debug("Found {0} application types", applicationTypes.Count);

            return applicationTypes;
        }

        #endregion

    }

}
