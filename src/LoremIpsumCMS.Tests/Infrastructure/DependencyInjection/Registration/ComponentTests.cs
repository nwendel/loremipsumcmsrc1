﻿using LoremIpsumCMS.Infrastructure.DependencyInjection;
using LoremIpsumCMS.Infrastructure.DependencyInjection.Registration;
using LoremIpsumCMS.Tests.Infrastructure.DependencyInjection.Registration.TestClasses;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public class ComponentTests : AbstractTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanResolve()
        {
            var serviceProvider = BuildServiceProvider(x =>
            {
                x.Register(Component
                    .For<ISimpleService>()
                    .ImplementedBy<SimpleService>());
            });

            var service = serviceProvider.GetService<ISimpleService>();
            Assert.NotNull(service);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRegisterMultipleAndResolveOne()
        {
            var serviceProvider = BuildServiceProvider(x =>
            {
                x.Register(Component
                    .For<ISimpleService>()
                    .ImplementedBy<SimpleService>());
                x.Register(Component
                    .For<ISimpleService>()
                    .ImplementedBy<SimpleService>());
            });

            var services = serviceProvider.GetServices<ISimpleService>();
            Assert.Equal(1, services.Count());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRegisterSingleton()
        {
            var serviceProvider = BuildServiceProvider(x =>
            {
                x.Register(Component
                    .For<ISimpleService>()
                    .ImplementedBy<SimpleService>());
            });

            var first = serviceProvider.GetService<ISimpleService>();
            var second = serviceProvider.GetService<ISimpleService>();
            Assert.Same(first, second);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRegisterSingletonWithMultipleInterfaces()
        {
            var serviceProvider = BuildServiceProvider(x =>
            {
                x.Register(Component
                    .ForAllInterfaces()
                    .ImplementedBy<ServiceWithTwoInterfaces>());
            });

            var first = serviceProvider.GetService<IServiceOne>();
            var second = serviceProvider.GetService<IServiceTwo>();
            Assert.Same(first, second);
        }


    }

}
