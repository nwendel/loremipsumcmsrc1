﻿namespace LoremIpsumCMS.Tests.Infrastructure.DependencyInjection.Registration.TestClasses
{

    /// <summary>
    /// 
    /// </summary>
    public class ServiceWithTwoInterfaces : IServiceOne, IServiceTwo
    {

        /// <summary>
        /// 
        /// </summary>
        public void OperationOne()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void OperationTwo()
        {
        }

    }

}
