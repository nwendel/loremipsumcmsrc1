﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace LoremIpsumCMS.Tests.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractTests
    {

        #region Build Service Provider

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        protected IServiceProvider BuildServiceProvider(Action<IServiceCollection> action)
        {
            var serviceCollection = new ServiceCollection();
            action(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            return serviceProvider;
        }

        #endregion

    }

}
