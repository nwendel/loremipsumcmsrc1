﻿using LoremIpsumCMS.Infrastructure.DependencyInjection;
using LoremIpsumCMS.Infrastructure.DependencyInjection.Registration;
using LoremIpsumCMS.Tests.Infrastructure.DependencyInjection.Registration.TestClasses;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Xunit;

namespace LoremIpsumCMS.Tests.Infrastructure.DependencyInjection.Registration
{

    /// <summary>
    /// 
    /// </summary>
    public class ClassesTests : AbstractTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanResolve()
        {
            var serviceProvider = BuildServiceProvider(x =>
            {
                x.Register(Classes
                    .FromThisAssembly()
                    .BasedOn<ISimpleService>()
                    .WithService.AllInterfaces());
            });

            var service = serviceProvider.GetService<ISimpleService>();
            Assert.NotNull(service);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRegisterMultipleAndResolveOne()
        {
            var serviceProvider = BuildServiceProvider(x =>
            {
                x.Register(Classes
                    .FromThisAssembly()
                    .BasedOn<ISimpleService>()
                    .WithService.AllInterfaces());
                x.Register(Classes
                    .FromThisAssembly()
                    .BasedOn<ISimpleService>()
                    .WithService.AllInterfaces());
            });

            var services = serviceProvider.GetServices<ISimpleService>();
            Assert.Equal(1, services.Count());
        }

    }

}
