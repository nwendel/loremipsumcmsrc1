﻿using LoremIpsumCMS.Infrastructure.Dependency;

namespace LoremIpsumCMS.Tests.Infrastructure.Dependency.Classes
{

    /// <summary>
    /// 
    /// </summary>
    [DependsOn(typeof(Two))]
    public class One
    {
    }

}
