﻿using LoremIpsumCMS.Infrastructure.Dependency;

namespace LoremIpsumCMS.Tests.Infrastructure.Dependency.Classes
{

    /// <summary>
    /// 
    /// </summary>
    [DependsOn(typeof(Circular))]
    public class Circular
    {
    }

}
