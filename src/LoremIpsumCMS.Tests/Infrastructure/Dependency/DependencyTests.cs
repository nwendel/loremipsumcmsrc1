﻿using System.Linq;
using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Tests.Infrastructure.Dependency.Classes;
using Xunit;

namespace LoremIpsum.Tests.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    public class DependencyTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNonExistantDependency()
        {
            var instances = new[] { new One() };

            Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderInDependencyOrderCorrectOrder()
        {
            var one = new One();
            var two = new Two();

            var instances = new object[] { one, two };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Equal(two, orderedInstances[0]);
            Assert.Equal(one, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderInDependencyOrderWrongOrder()
        {
            var one = new One();
            var two = new Two();

            var instances = new object[] { two, one };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Equal(two, orderedInstances[0]);
            Assert.Equal(one, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanIgnoreOptionalDependency()
        {
            var three = new Three();

            var instances = new object[] { three };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(1, orderedInstances.Count());
            Assert.Equal(three, orderedInstances[0]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderOptionalInDependencyOrderCorrectOrder()
        {
            var three = new Three();
            var two = new Two();

            var instances = new object[] { three, two };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Equal(two, orderedInstances[0]);
            Assert.Equal(three, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderOptionalInDependencyOrderWrongOrder()
        {
            var three = new Three();
            var two = new Two();

            var instances = new object[] { two, three };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Equal(two, orderedInstances[0]);
            Assert.Equal(three, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCircularDependencyOnSelf()
        {
            var instances = new[] { new Circular() };

            Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCircularDependencyThree()
        {
            var instances = new object[] { new CircleOne(), new CircleTwo(), new CircleThree() };

            Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCircularDependencyFour()
        {
            var instances = new object[] { new CircleFour(), new Circular() };

            Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
        }

    }

}
