﻿using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Modules;
using LoremIpsumCMS.Web.Mvc.Routing;
using Microsoft.AspNet.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace LoremIpsumCMS.Web
{

    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationBuilderExtensions
    {

        private static IServiceProvider _serviceProvider;
        private static ILogger _logger;


        #region Use Lorem Ipsum

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static IApplicationBuilder UseLoremIpsum(this IApplicationBuilder self)
        {
            _serviceProvider = self.ApplicationServices.GetService<IServiceProvider>();
            _logger = _serviceProvider.GetService<ILogger<IApplicationBuilder>>();

            //self = self.UseMiddleware<RoutingMiddleware>();

            ConfigureModules(self);
            Initialize(self);

            return self;
        }

        #endregion

        #region Configure Modules

        /// <summary>
        /// 
        /// </summary>
        private static void ConfigureModules(IApplicationBuilder applicationBuilder)
        {
            var modules = _serviceProvider.GetServices<IModule>();
            foreach (var module in modules)
            {
                _logger.LogDebug("Configuring module {0}", module.GetType().FullName);
                module.Configure(applicationBuilder);
            }
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        private static void Initialize(IApplicationBuilder applicationBuilder)
        {
            var initializers = _serviceProvider.GetServices<IInitializer>();
            _logger.LogDebug("{0} initializers found", initializers.Count());
            var applicationBuilderInitializers = _serviceProvider.GetServices<IApplicationBuilderInitializer>();
            _logger.LogDebug("{0} application builder initializers found", initializers.Count());

            var allInitializers = initializers
                .Cast<object>()
                .Concat(applicationBuilderInitializers)
                .ToList();

            var orderedInitializers = allInitializers.OrderByDependencies();
            foreach (var initializer in orderedInitializers)
            {
                _logger.LogDebug("Before initialize {0}", initializer.GetType().Name);
                (initializer as IInitializer)?.Initialize();
                (initializer as IApplicationBuilderInitializer)?.Initialize(applicationBuilder);
                _logger.LogDebug("After initialize {0}", initializer.GetType().Name);
            }
        }

        #endregion
    }

}
