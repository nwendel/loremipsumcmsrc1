﻿using LoremIpsumCMS.Infrastructure.DependencyInjection;
using LoremIpsumCMS.Infrastructure.DependencyInjection.Registration;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Initializer;
using LoremIpsumCMS.Web.Infrastructure.Modules;
using LoremIpsumCMS.Web.Mvc.Routing;
using LoremIpsumCMS.Web.Mvc.Routing.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Web
{

    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {

        #region Use Lorem Ipsum

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static void AddLoremIpsum(this IServiceCollection self)
        {
            //self.Register(Component
            //    .For<IModuleLoader>()
            //    .ImplementedBy<ModuleLoader>());

            self.Register(Component
                .ForAllInterfaces()
                .ImplementedBy<ModuleLoader>());

            self.AddSingleton<IModuleLoader, ModuleLoader>();

            self.Register(Classes
                .FromThisAssembly()
                .Where(Component.IsInSameNamespaceAs<RouteParameterService>())
                .WithService.AllInterfaces());

            self.Register(Classes
                .FromThisAssembly()
                .BasedOn<IInitializer>()
                .WithService.AllInterfaces());
            self.Register(Classes
                .FromThisAssembly()
                .BasedOn<IApplicationBuilderInitializer>()
                .WithService.AllInterfaces());

            self.Register(Component
                .For<PageRouter>()
                .ImplementedBy<PageRouter>());

            var serviceProvider = self.BuildServiceProvider();
            var moduleTypes = LoadModules(serviceProvider);
            ConfigureModuleServices(self, moduleTypes);
        }

        #endregion

        #region Load Modules

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        private static IEnumerable<Type> LoadModules(IServiceProvider serviceProvider)
        {
            var moduleLoader = serviceProvider.GetService<IModuleLoader>();
            var moduleTypes = moduleLoader.LoadAll();
            return moduleTypes;
        }

        #endregion

        #region Configure Module Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        /// <param name="moduleTypes"></param>
        private static void ConfigureModuleServices(IServiceCollection serviceCollection, IEnumerable<Type> moduleTypes)
        {
            var modules = moduleTypes
                .Select(x => Activator.CreateInstance(x))
                .Cast<IModule>();
            foreach (var module in modules)
            {
                serviceCollection.AddInstance(module);
                module.ConfigureServices(serviceCollection);
            }
        }

        #endregion

    }

}


