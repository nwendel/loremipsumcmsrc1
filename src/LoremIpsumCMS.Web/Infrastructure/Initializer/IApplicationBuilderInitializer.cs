﻿using Microsoft.AspNet.Builder;

namespace LoremIpsumCMS.Web.Infrastructure.Initializer
{

    /// <summary>
    /// 
    /// </summary>
    interface IApplicationBuilderInitializer
    {

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        void Initialize(IApplicationBuilder applicationBuilder);

        #endregion

    }

}
