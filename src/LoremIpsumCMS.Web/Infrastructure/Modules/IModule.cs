﻿using Microsoft.AspNet.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Web.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public interface IModule
    {

        #region Configure Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        void ConfigureServices(IServiceCollection serviceCollection);

        #endregion

        #region Configure

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        void Configure(IApplicationBuilder applicationBuilder);

        #endregion

    }

}
