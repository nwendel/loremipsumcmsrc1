﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNet.Builder;

namespace LoremIpsumCMS.Web.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractModule : IModule
    {

        #region Configure Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public virtual void ConfigureServices(IServiceCollection serviceCollection)
        {
        }

        #endregion

        #region Configure

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        public virtual void Configure(IApplicationBuilder applicationBuilder)
        {
        }

        #endregion

    }

}
