﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Web.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleLoader : IModuleLoader
    {

        #region Dependencies

        private ILogger _logger;
        private ILibraryManager _libraryManager;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyLoaderContainer"></param>
        /// <param name="assemblyLoadContextAccessor"></param>
        /// <param name="libraryManager"></param>
        public ModuleLoader(
            ILogger<ModuleLoader> logger,
            ILibraryManager libraryManager)
        {
            _logger = logger;
            _libraryManager = libraryManager;
        }

        #endregion

        #region Load All

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Type> LoadAll()
        {
            var assemblyName = typeof(ModuleLoader).Assembly.GetName().Name;

            var assemblies = _libraryManager.GetReferencingLibraries(assemblyName)
                          .SelectMany(info => info.Assemblies)
                          .Select(info => Assembly.Load(new AssemblyName(info.Name)))
                          .ToList();
            foreach(var assembly in assemblies)
            {
                _logger.LogDebug("Found Module Assembly: {0}", assembly.FullName);
            }

            var moduleTypes = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => !x.IsAbstract && typeof(IModule).IsAssignableFrom(x))
                .ToList();
            foreach(var moduleType in moduleTypes)
            {
                _logger.LogDebug("Found module: {0}", moduleType.FullName);
            }

            return moduleTypes;
        }

        #endregion

    }

}
