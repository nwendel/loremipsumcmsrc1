﻿using System;
using System.Collections.Generic;

namespace LoremIpsumCMS.Web.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public interface IModuleLoader
    {

        /// <summary>
        /// 
        /// </summary>
        IEnumerable<Type> LoadAll();

    }

}
