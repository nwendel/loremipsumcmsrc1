﻿using LoremIpsumCMS.Model;
using System;

namespace LoremIpsum.Web.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouteForAttribute : Attribute
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public PageRouteForAttribute(Type dataType)
        {
            if(dataType == null)
            {
                throw new ArgumentNullException(nameof(dataType));
            }
            if (!typeof(AbstractPageContentItemData).IsAssignableFrom(dataType))
            {
                throw new ArgumentException(string.Format("{0} must inherit {1}", DataType.FullName, typeof(AbstractPageContentItemData).FullName), nameof(dataType));
            }

            DataType = dataType;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Type DataType { get; private set; }

        #endregion

    }

}