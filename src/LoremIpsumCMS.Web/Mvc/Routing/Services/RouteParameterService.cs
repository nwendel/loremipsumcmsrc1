﻿using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Routing;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace LoremIpsumCMS.Web.Mvc.Routing.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteParameterService : IRouteParameterService
    {

        #region Analyze Method

        private Dictionary<Type, RouteParameterType> _parameterTypeLookup = new Dictionary<Type, RouteParameterType>
        {
            { typeof(Site), RouteParameterType.Site },
            { typeof(PageContentItem), RouteParameterType.PageContentItem },
            { typeof(PageReference), RouteParameterType.PageReference }
        };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <returns></returns>
        public IEnumerable<RouteParameter> AnalyzeMethod(MethodInfo methodInfo, Type dataType)
        {
            var parameters = methodInfo.GetParameters();
            foreach (var parameter in parameters)
            {
                var routeParameter = new RouteParameter { Name = parameter.Name };

                if (parameter.ParameterType == dataType)
                {
                    routeParameter.Type = RouteParameterType.PageData;
                }
                else if (_parameterTypeLookup.ContainsKey(parameter.ParameterType))
                {
                    routeParameter.Type = _parameterTypeLookup[parameter.ParameterType];
                }
                else
                {
                    // No exception, there could be other things which binds parameters...
                    continue;
                }

                yield return routeParameter;
            }
        }

        #endregion

    }

}
