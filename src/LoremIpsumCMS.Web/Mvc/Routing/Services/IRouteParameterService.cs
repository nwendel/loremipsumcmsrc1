﻿using LoremIpsumCMS.Web.Routing;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace LoremIpsumCMS.Web.Mvc.Routing.Services
{

    /// <summary>
    /// 
    /// </summary>
    public interface IRouteParameterService
    {

        #region Analyze Method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <returns></returns>
        IEnumerable<RouteParameter> AnalyzeMethod(MethodInfo methodInfo, Type dataType);

        #endregion

    }

}
