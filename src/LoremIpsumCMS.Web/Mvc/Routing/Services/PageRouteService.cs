﻿using LoremIpsum.Web.Routing;
using LoremIpsumCMS.Infrastructure.Initializer;
using LoremIpsumCMS.Reflection;
using LoremIpsumCMS.Web.Routing;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Web.Mvc.Routing.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouteService : IPageRouteService, IInitializer
    {

        #region Fields

        private readonly Dictionary<Type, Type> _pageDataTypeToControllerType = new Dictionary<Type, Type>();
        private readonly Dictionary<Type, string> _pageDataTypeToActionName = new Dictionary<Type, string>();
        private readonly Dictionary<Type, IEnumerable<RouteParameter>> _pageDataTypeToRouteParameters = new Dictionary<Type, IEnumerable<RouteParameter>>();

        #endregion

        #region Dependencies

        private readonly ILogger _logger;
        private readonly IRouteParameterService _routeParameterService;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeParameterService"></param>
        public PageRouteService(
            ILogger<PageRouteService> logger,
            IRouteParameterService routeParameterService)
        {
            _logger = logger;
            _routeParameterService = routeParameterService;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            var types = ReflectionUtility.GetApplicationAndFrameworkTypes();
            var controllerTypes = types.Where(t => !t.IsAbstract && typeof(Controller).IsAssignableFrom(t));

            foreach (var controllerType in controllerTypes)
            {
                foreach (var methodInfo in controllerType.GetMethods())
                {
                    if (IsPageRoute(methodInfo))
                    {
                        var dataType = methodInfo.GetCustomAttribute<PageRouteForAttribute>().DataType;
                        var actionName = methodInfo.Name;
                        var routeParameters = _routeParameterService.AnalyzeMethod(methodInfo, dataType).ToList();

                        _logger.LogDebug("Found page controller action");
                        _logger.LogDebug("ControllerType: {0}", controllerType.Name);
                        _logger.LogDebug("ActionName: {0}", actionName);
                        _logger.LogDebug("DataType: {0}", dataType.Name);

                        _pageDataTypeToControllerType.Add(dataType, controllerType);
                        _pageDataTypeToActionName.Add(dataType, actionName);
                        _pageDataTypeToRouteParameters.Add(dataType, routeParameters);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <returns></returns>
        private static bool IsPageRoute(MethodInfo methodInfo)
        {
            if (methodInfo.ReturnParameter == null)
            {
                return false;
            }

            var isPageRoute = methodInfo.IsPublic &&
                              typeof(IActionResult).IsAssignableFrom((methodInfo.ReturnParameter.ParameterType)) &&
                              methodInfo.GetCustomAttributes<PageRouteForAttribute>().Any();
            return isPageRoute;
        }

        #endregion

    }

}
