﻿namespace LoremIpsumCMS.Web.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public enum RouteParameterType
    {
        Site = 0,

        PageContentItem = 1,
        PageData = 2,
        PageReference = 3
    }

}