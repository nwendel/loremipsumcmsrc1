﻿namespace LoremIpsumCMS.Web.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteParameter
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RouteParameterType Type { get; set; }

        #endregion

    }

}