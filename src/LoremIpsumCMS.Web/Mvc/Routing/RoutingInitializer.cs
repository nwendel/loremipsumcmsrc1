﻿using LoremIpsumCMS.Web.Infrastructure.Initializer;
using Microsoft.AspNet.Builder;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class RoutingInitializer : IApplicationBuilderInitializer
    {

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize(IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<RoutingMiddleware>();
        }

        #endregion

    }

}
