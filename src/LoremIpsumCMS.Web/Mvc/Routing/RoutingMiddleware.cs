﻿using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Routing;
using System.Threading.Tasks;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class RoutingMiddleware
    {

        #region Dependencies

        private readonly RequestDelegate _next;
        private readonly PageRouter _pageRouter;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        /// <param name="pageRouter"></param>
        public RoutingMiddleware(
            RequestDelegate next,
            PageRouter pageRouter)
        {
            _next = next;
            _pageRouter = pageRouter;
        }

        #endregion

        #region Invoke

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext httpContext)
        {
            var routeContext = new RouteContext(httpContext);
            routeContext.RouteData.Routers.Add(_pageRouter);

            await _pageRouter.RouteAsync(routeContext);

            if (!routeContext.IsHandled)
            {
                await _next(httpContext);
            }
        }

        #endregion

    }

}
