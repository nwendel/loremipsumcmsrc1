﻿using System.Threading.Tasks;
using Microsoft.AspNet.Routing;
using Microsoft.AspNet.Mvc.Infrastructure;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouter : IRouter
    {

        #region Route Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task RouteAsync(RouteContext context)
        {
            var handler = new MvcRouteHandler();
            await handler.RouteAsync(context);
        }

        #endregion

        #region Get Virtual Path

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            // TODO: Something like this if the path matches a page otherwise null...

            //var result = new VirtualPathData(this, page.VirtualPath);
            //context.IsBound = true;
            //return result;

            return null;
        }

        #endregion

    }

}
